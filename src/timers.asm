;   Timers - Play with the PIC timers
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of Timers
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;Timers (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; The display leds are connected to:
; - RB0 - 6 - bit 0 of the counter
; - RB1 - 7 - bit 1 of the counter
; - RB2 - 8 - bit 2 of the counter
; - RB3 - 9 - bit 3 of the counter
; - RB4 - 10 - bit 4 of the counter
; - RB5 - 11 - bit 5 of the counter
; - RB6 - 12 - bit 6 of the counter
; - RB7 - 13 - bit 7 of the counter
; - RA0 - 17 - switch counter 0 button
; - RA1 - 18 - switch counter 1 button
; - RA2 - 1 - switch counter 2 button
;=============================

;Variables
;==============================
COUNTER EQU 0x27

;Assembler directives
;==============================
	list p=16f628a
        include p16f628a.inc
        __config b'11111100111000'
        org 0x00 ;Reset vector
        goto Start
	org 0x04 ;Interrupt vector
	goto Interrupt

;Functions
;==============================
	include digitecnology.inc
SetTimer0
	call UnSetTimer1
	call UnSetTimer2
	call chgbnk1
	bcf OPTION_REG,5 ;Use clock oscillator to TIMER0
	bcf OPTION_REG,3 ;Assign prescaler to TIMER0
	bsf OPTION_REG,0
	bsf OPTION_REG,1
	bsf OPTION_REG,2 ;Prescaler 1:256
	call chgbnk0
	bsf INTCON,5 ;Enable TIMER0 interrupt
	bsf INTCON,7 ;Enable interrupts
	clrf COUNTER
	clrf TMR0 ;Clear TIMER0 register
	return
UnSetTimer0
	call chgbnk0
	bcf INTCON,5 ;Disable TIMER0 interrupt
	bcf INTCON,7 ;Disable interrupts
	return
SetTimer1
	call UnSetTimer0
	call UnSetTimer2
	call chgbnk0
	bsf T1CON,5 ;Set prescaler to 1:8
	bsf T1CON,4
	bcf T1CON,3 ;Disable external oscilator
	bcf T1CON,1 ;Use internal clock
	bsf T1CON,0 ;Enables TIMER1
	bsf INTCON,6 ;Enable peripherl interrupts
	call chgbnk1
	bsf PIE1,0 ;Enables TIMER1 interrupt
	call chgbnk0	
	bsf INTCON,7 ;Enables interrupts
	clrf COUNTER
	clrf TMR1L ;Clears the TIMER1
	clrf TMR1H
	return
UnSetTimer1
	call chgbnk0
	bcf T1CON,0 ;Disable TIMER1
	bcf INTCON,6 ;Disables peripheral interrupts
	call chgbnk1
	bcf PIE1,0 ;Disable TIMER1 interrupt
	call chgbnk0
	bcf INTCON,7 ;Disable interrupts
	return
SetTimer2
	call UnSetTimer0
	call UnSetTimer1
	call chgbnk0
	movlw 0xFF ;Assign 255 to PR2
	movwf PR2
	bsf T2CON,6 ;Assign a 1:16 postscaler
	bsf T2CON,5
	bsf T2CON,4
	bsf T2CON,3
	bsf T2CON,1 ;Assign a 1:16 prescaler
	bsf T2CON,0
	bsf T2CON,2 ;Set TIMER2 on
	bsf INTCON,6 ;Enable peripherl interrupts
        call chgbnk1
        bsf PIE1,1 ;Enables TIMER2 interrupt
        call chgbnk0
        bsf INTCON,7 ;Enables interrupts
        clrf COUNTER
	clrf TMR2 ;Clears the TIMER2		
	return
UnSetTimer2
	call chgbnk0
	bcf T2CON,2 ;Set TIMER2 off
	bcf INTCON,6 ;Disables peripheral interrupts
        call chgbnk1
        bcf PIE1,1 ;Disable TIMER2 interrupt
        call chgbnk0
        bcf INTCON,7 ;Disable interrupts	
	return
;Program
;==============================
Interrupt ;Interrupt controls
	bcf INTCON,7 ;Disable interrupts
	call chgbnk0
	btfss INTCON,2 ;Check if TIMER0 interrupt
	goto $+3	
	incf COUNTER,1
	bcf INTCON,2 ;Clear interrupt	
	btfss PIR1,0 ;Check if TIMER1 interrupt
        goto $+3
	incf COUNTER,1	
	bcf PIR1,0 ;Clear interrupt
	btfss PIR1,1 ;Check if TIMER2 interrupt
	goto $+3
        incf COUNTER,1
        bcf PIR1,1 ;Clear interrupt
	movf COUNTER,0 ;Set LED's
        movwf PORTB ;Show counter
	bsf INTCON,7 ;Enable interrupts
	retfie
Start ;Main program
	goto Initialize
Initialize
	;Change to bank 0
	call chgbnk0
	;Deactivate analog comparators
	bsf CMCON,2
	bsf CMCON,1
	bsf CMCON,0
	;Clear PORTA and PORTB
	clrf PORTA
	clrf PORTB
	;Change to bank 1
	call chgbnk1
	;Set inputs/outputs
	clrf TRISB ;All outputs
	bsf TRISA,0 ;Input
	bsf TRISA,1 ;Input
	bsf TRISA,2 ;Input
	;Change to bank 0
	call chgbnk0
	;Set TIMER0 as default
	call SetTimer0
Cycle
	;Check buttons
	call chgbnk0
	btfsc PORTA,0 ;Button 0
	goto $+2
	call SetTimer0
	btfsc PORTA,1 ;Button 1
	goto $+2
	call SetTimer1
	btfsc PORTA,2 ;Button 2
        goto $+2
        call SetTimer2
	goto Cycle
	end
